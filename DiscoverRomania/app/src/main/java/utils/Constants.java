package utils;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static final String IS_ADDED_INDB = "is_added_in_db";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_STATUS = "user_status";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_DATE_JOINED = "user_date_joined";
    public static final String USER_IMAGE = "user_image";
    public static final String USER_ID = "user_id";
    public static final String OTHER_USER_ID = "other_user_id";
    public static final String READ_NOTIF_STATUS = "read_notificatons_status";

    public static final String EVENT_IMG = "event_name";
    public static final String EVENT_NAME = "event_img";
    public static final String IMAGE_NO = "number";


    public static final List<String> BASE_TASKS = new ArrayList<String>() {
        {
            add("Bags");
            add("Keys");
            add("Charger");
            add("Earphones");
            add("Clothes");
            add("Food");
            add("Tickets");
        }
    };


    public static final List<String> NICKNAME_CITY = new ArrayList<String>() {
        {
            add("Bucharest");
            add("Brasov");
            add("Cluj Napoca");
            add("Constanta");
            add("Iasi");
            add("Oradea");
            add("Suceava");
            add("Timisoara");
            add("Targu Mures");
            add("Alba Iulia");
        }
    };

    public static final List<String> IMAGE_CITY = new ArrayList<String>() {
        {
            add("https://i.imgur.com/X0VAjK0.jpg");
            add("https://i.imgur.com/8zmE8Ow.jpeg");
            add("https://i.imgur.com/neaK4WE.jpeg");
            add("https://i.imgur.com/WdijCWx.jpeg");
            add("https://i.imgur.com/Tz0h0hz.jpg");
            add("https://i.imgur.com/JlbkEZ6.jpeg");
            add("https://i.imgur.com/uVcR7r4.jpg");
            add("https://i.imgur.com/HjAW0Yi.jpg");
            add("https://bit.ly/331v9K6");
            add("https://bit.ly/3k3XhmP");
        }
    };

    public static final List<String> DESCRIPTION_CITY = new ArrayList<String>() {
        {
            add("Bucharest is the capital and largest city of Romania, as well as its cultural, industrial, and financial centre. It has a growing cultural scene, in fields including the visual arts, performing arts and nightlife.");
            add("Brasov, lies in the centre of Romania. The origins seem to be lost long ago in history: the first document dating from 1235. One can still see and visit today some of the old fortifications in the city. The buildings and the streets in the historical centre still preserve a medieval ambiance and they are a favourite tourist attraction.");
            add("Cluj Napoca commonly known as Cluj, is the fourth most populous city in Romania and the seat of Cluj County in the northwestern part of the country. The city is considered the unofficial capital to the historical province of Transylvania. Today, the city is one of the most important academic, cultural, industrial and business centres in Romania.  Cluj-Napoca held the titles of European Youth Capital in 2015 and European City of Sport in 2018.");
            add("An ancient metropolis, Romania's oldest continuously inhabited and the country's largest sea port, Constanta traces its history some 2,500 years.The third largest city in Romania, Constanta is now an important cultural and economic centre, worth exploring for its archaeological treasures and the Old Town's architecture. Its historical monuments, ancient ruins, grand Casino, museums and shops, and proximity to beach resorts make it the focal point of Black Sea coast tourism. Open-air restaurants, nightclubs and cabarets offer a wide variety of entertainment.");
            add("Iași is the second largest city in Romania, and the seat of Iași County. The social and cultural life revolves around the Vasile Alecsandri National Theater (the oldest in Romania), the Moldova State Philharmonic, the Opera House, the Iași Athenaeum, a famous Botanical Garden (the oldest and largest in Romania), the Central University Library (the oldest in Romania), the high quality cultural centres and festivals, an array of museums, memorial houses, religious and historical monuments.");
            add("Chief town of Bihor, one of the 41 counties of Romania, and located in the historical regions of Transylvania and Partium, Oradea is considered the gateway to central and western Europe. Today it is one of the most important economic, social and cultural centres of Romania, thanks to the presence of a prestigious university, the largest in Eastern Europe, which excels in the fields of literature, medicine and science.");
            add("Suceava is the largest city and the seat of Suceava County, situated in the historical region of Bukovina, north-eastern Romania, and at the crossroads of Central and Eastern Europe");
            add("With its rich history, vibrant cultural scene, lively terraces and cafés and robust student population, Timișoara, the 2021 European Capital of Culture is Romania’s must-visit city right now. ");
            add("Targu Mures enjoys the best of both Romanian and Hungarian cultures. Numerous vestiges attest the presence of Neolithic cultures and those of the Bronze and Metal Ages in this area.Today its centrally located Piata Trandafirilor (Roses Square) is lined with modern streetside cafes and restaurants, churches, and monuments. Targu Mures' top attraction is located at the south end of the square: the Culture Palace (Palatul Culturii), a flamboyant early 20th-century city hall with an outstanding stained-glass hall, housing some of main local museums.");
            add("One of the oldest settlements in Romania, known in ancient time as Apulum, Alba Iulia served as the largest military and economic center during the Roman occupation. In the old town visitors can stroll along the wide, tree-lined streets of the Habsburg citadel, one of the most impressive in Europe, to discover the historical, cultural and architectural places of interest of Alba Iulia: the Roman Catholic Cathedral – the oldest and most valuable monument of architecture in Transylvania.");
        }
    };

    public static final List<String> LATITUDE_CITY = new ArrayList<String>() {
        {
            add("44.439663");
            add("45.657974");
            add("46.770439");
            add("44.179249");
            add("47.151726");
            add("47.0667");
            add("47.6635");
            add("45.760696");
            add("46.54245");
            add("46.0632");
        }
    };

    public static final List<String> LONGITUDE_CITY = new ArrayList<String>() {
        {
            add("26.096306");
            add("25.601198");
            add("23.591423");
            add("28.649940");
            add("27.587914");
            add("21.9333");
            add("26.2732");
            add("21.226788");
            add("24.55747");
            add("23.5626");

        }
    };

    // For passing within intents / fragments
    public static final String EXTRA_MESSAGE_TYPE = "type_";
    public static final String EXTRA_MESSAGE_CITY_OBJECT = "cityobject_";
    public static final String EXTRA_MESSAGE_IMAGE_URI = "profileimageuri_";
    public static final String EXTRA_MESSAGE_USER_FULLNAME = "userfullname_";

}