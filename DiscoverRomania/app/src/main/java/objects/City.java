package objects;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity
public class City implements Serializable {

    //so that primary key is generated automatically
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    //   private final List<String> mInterests = new ArrayList<>();

    @ColumnInfo(name = "imageurl")
    @Nullable
    private String mAvatar;

    @ColumnInfo(name = "name")
    private String mNickname;

    @ColumnInfo(name = "description")
    @Nullable
    private String mDescription;

    @ColumnInfo(name = "latitude")
    @Nullable
    private String mLatitude;

    @ColumnInfo(name = "longitude")
    @Nullable
    private String mLongitude;

    private int mBackgroundColor;


    @Ignore
    public City(String mAvatar, String mNickname, int mId, int mBackgroundColor, String mDescription, String mLatitude, String mLongitude) {
        this.mAvatar = mAvatar;
        this.mNickname = mNickname;
        this.mId = mId;
        this.mBackgroundColor = mBackgroundColor;
        this.mDescription = mDescription;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
    }

    @Ignore
    public City(String nickname, int id) {
        this.mNickname = nickname;
        this.mId = id;
    }


    @Ignore
    public City(int id, String nickname, String avatar, String description) {
        this.mId = id;
        this.mNickname = nickname;
        this.mAvatar = avatar;
        this.mDescription = description;
    }

    public City(int id, String nickname, String avatar, String description, String latitude, String longitude) {
        this.mId = id;
        this.mNickname = nickname;
        this.mAvatar = avatar;
        this.mDescription = description;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }


    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String mAvatar) {
        this.mAvatar = mAvatar;
    }

    public String getNickname() {
        return mNickname;
    }

    public void setNickname(String mNickname) {
        this.mNickname = mNickname;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }


    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    public void setBackgroundColor(int mBackgroundColor) {
        this.mBackgroundColor = mBackgroundColor;
    }
}