package objects;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class User {

    //so that primary key is generated automatically

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId = 0;


    @ColumnInfo(name = "username")
    private String mUsername;


    @ColumnInfo(name = "firstname")
    private String mFirstName;

    @ColumnInfo(name = "lastname")
    private String mLastName;

    //  private String mImage;
    @ColumnInfo(name = "datejoined")
    private String mDateJoined;

    @ColumnInfo(name = "status")
    private String mStatus;

    @ColumnInfo(name = "image")
    private String mImage;


    @ColumnInfo(name = "password")
    private String mPassword;

    @ColumnInfo(name = "token")
    private String mUserToken;

    @ColumnInfo(name = "loginType")
    private String mLoginType;


    public User(int mId, String mUsername, String mFirstName, String mLastName,
                String mImage, String mDateJoined, String mStatus, String mPassword) {
        this.mUsername = mUsername;
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mId = mId;
        this.mImage = mImage;
        this.mDateJoined = mDateJoined;
        this.mStatus = mStatus;
        this.mPassword = mPassword;
    }


    public User(String userName, String pass) {
        this.mUsername = userName;
        this.mPassword = pass;
    }

    public User(String userName, String pass, String firstName, String lastName, String dateJoined, String img) {
        this.mUsername = userName;
        this.mPassword = pass;
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mDateJoined = dateJoined;
        this.mImage = img;
    }

    public User(String userName, String pass, String firstName, String lastName, String dateJoined, String img, String token, String loginType) {
        this.mUsername = userName;
        this.mPassword = pass;
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mDateJoined = dateJoined;
        this.mImage = img;
        this.mUserToken = token;
        this.mLoginType = loginType;
    }

    public User(String userName, String pass, String firstName, String lastName, String dateJoined) {
        this.mUsername = userName;
        this.mPassword = pass;
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mDateJoined = dateJoined;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        this.mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public int getId() {
        return mId;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String imgUrl) {
        this.mImage = imgUrl;
    }

    public String getDateJoined() {
        return mDateJoined;
    }

    public void setDateJoined(String date) {
        this.mDateJoined = date;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        this.mStatus = status;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getUserToken() {
        return mUserToken;
    }

    public void setUserToken(String token) {
        this.mUserToken = token;
    }

    public String getLoginType() {
        return mLoginType;
    }

    public void setLoginType(String loginType) {
        this.mLoginType = loginType;
    }
}
