package objects;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Model class for city object
 */
@Entity(tableName = "events_new")
public class ChecklistItem {

    @ColumnInfo(name = "name")
    private final String mName;
    @ColumnInfo(name = "isDone")
    private final String mIsDone;
    //so that primary key is generated automatically
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId = 0;

    /**
     * Initiates checklist item
     *
     * @param name   checklist task name
     * @param isDone specify if the item is checked
     */

    public ChecklistItem(String name, String isDone) {
        this.mName = name;
        this.mIsDone = isDone;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public String getIsDone() {
        return mIsDone;
    }
}