package discover;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.example.discoverromania.R;
import com.example.discoverromania.destinations.description.FinalCityInfoActivity;
import com.example.discoverromania.roompersistence.DBDiscoverRomania;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowCloseListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import objects.City;

/**
 * This shows how to place markers on a map.
 */
public class MarkerDemoActivity extends AppCompatActivity implements
        OnMarkerClickListener,
        OnInfoWindowClickListener,
        OnMarkerDragListener,
        OnSeekBarChangeListener,
        OnInfoWindowLongClickListener,
        OnInfoWindowCloseListener,
        OnMapAndViewReadyListener.OnGlobalLayoutAndMapReadyListener,
        OnMapReadyCallback,
        GoogleMap.OnGroundOverlayClickListener {

    private static final int TRANSPARENCY_MAX = 100;
    private final LatLng PARLAMENT = new LatLng(44.427252281206485, 26.087808609008793);
    private final List<Marker> mMarkerCity = new ArrayList<Marker>();
    private final Random mRandom = new Random();
    private final List<BitmapDescriptor> images = new ArrayList<BitmapDescriptor>();
    DBDiscoverRomania dataBase = DBDiscoverRomania.getsInstance(this);
    List<String> latList = dataBase.cityDAO().getCityLat();
    List<String> longList = dataBase.cityDAO().getCityLong();
    private final LatLng BUCHAREST = new LatLng(Double.parseDouble(latList.get(0)), Double.parseDouble(longList.get(0)));
    private final LatLng BRASOV = new LatLng(Double.parseDouble(latList.get(1)), Double.parseDouble(longList.get(1)));
    private final LatLng CLUJ = new LatLng(Double.parseDouble(latList.get(2)), Double.parseDouble(longList.get(2)));
    private final LatLng CONSTANTA = new LatLng(Double.parseDouble(latList.get(3)), Double.parseDouble(longList.get(3)));
    private final LatLng IASI = new LatLng(Double.parseDouble(latList.get(4)), Double.parseDouble(longList.get(4)));
    private final LatLng ORADEA = new LatLng(Double.parseDouble(latList.get(5)), Double.parseDouble(longList.get(5)));
    private final LatLng SUCEAVA = new LatLng(Double.parseDouble(latList.get(6)), Double.parseDouble(longList.get(6)));
    private final LatLng TIMISOARA = new LatLng(Double.parseDouble(latList.get(7)), Double.parseDouble(longList.get(7)));
    private final LatLng TARGUMURES = new LatLng(Double.parseDouble(latList.get(8)), Double.parseDouble(longList.get(8)));
    private final LatLng ALBAIULIA = new LatLng(Double.parseDouble(latList.get(9)), Double.parseDouble(longList.get(9)));
    private GoogleMap mMap;
    private Marker mOradea;
    private Marker mConstanta;
    private Marker mBucharest;
    private Marker mIasi;
    private Marker mBrasov;
    private Marker mTimis;
    private Marker mAlba;
    private Marker mTgMures;
    private Marker mSuceava;
    private Marker mCluj;
    /**
     * Keeps track of the last selected marker (though it may no longer be selected).  This is
     * useful for refreshing the info window.
     */
    private Marker mLastSelectedMarker;
    private TextView mTopText;
    private CheckBox mFlatBox;
    private RadioGroup mOptions;
    private SeekBar transparencyBar;
    private int currentEntry = 0;
    private GroundOverlay groundOverlay;
    private GroundOverlay groundOverlayRotated;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MarkerDemoActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.marker_demo);

        mTopText = (TextView) findViewById(R.id.top_text);

        mFlatBox = (CheckBox) findViewById(R.id.flat);
        transparencyBar = findViewById(R.id.transparencySeekBar);
        transparencyBar.setMax(TRANSPARENCY_MAX);
        transparencyBar.setProgress(0);

        mOptions = (RadioGroup) findViewById(R.id.custom_info_window_options);
        mOptions.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (mLastSelectedMarker != null && mLastSelectedMarker.isInfoWindowShown()) {
                    // Refresh the info window when the info window's content has changed.
                    mLastSelectedMarker.showInfoWindow();
                }
            }
        });

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        new OnMapAndViewReadyListener(mapFragment, this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        // Hide the zoom controls as the button panel will cover it.
        mMap.getUiSettings().setZoomControlsEnabled(false);

        // Add lots of markers to the map.
        addMarkersToMap();

        // Setting an info window adapter allows us to change the both the contents and look of the
        // info window.
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

        // Set listeners for marker events.  See the bottom of this class for their behavior.
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnInfoWindowCloseListener(this);
        mMap.setOnInfoWindowLongClickListener(this);

        // Override the default content description on the view, for accessibility mode.
        // Ideally this string would be localised.

        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(ORADEA)
                .include(CONSTANTA)
                .include(IASI)
                .include(BUCHAREST)
                .include(BRASOV)
                .include(CLUJ)
                .include(TARGUMURES)
                .include(ALBAIULIA)
                .include(TIMISOARA)
                .include(SUCEAVA)
                .build();
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));

        images.clear();
        images.add(BitmapDescriptorFactory.fromResource(R.drawable.buc_metro));
        images.add(BitmapDescriptorFactory.fromResource(R.drawable.parlament));

        // Add a small, rotated overlay that is clickable by default
        // (set by the initial state of the checkbox.)
        groundOverlayRotated = map.addGroundOverlay(new GroundOverlayOptions()
                .image(images.get(1)).anchor((float) 0.7, (float) 0.3)
                .position(PARLAMENT, 750f, 420f)
                .bearing(7)
        );

        // Add a large overlay at Bucharest on top of the smaller overlay.
        groundOverlay = map.addGroundOverlay(new GroundOverlayOptions()
                .image(images.get(currentEntry)).anchor((float) 0.5, (float) 0.60)
                .position(BUCHAREST, 17500f, 16500f));
        transparencyBar.setOnSeekBarChangeListener(this);

    }

    private void addMarkersToMap() {
        boolean flat = mFlatBox.isChecked();
        // Uses a colored icon.
        mBucharest = mMap.addMarker(new MarkerOptions()
                .position(BUCHAREST)
                .title("Bucharest")
                .snippet("Population: 2,151,665")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .flat(flat));
        mMarkerCity.add(mBucharest);
        // Uses a custom icon with the info window popping out of the center of the icon.
        mConstanta = mMap.addMarker(new MarkerOptions()
                .position(CONSTANTA)
                .title("Constanta")
                .snippet("Population: 317,832")
                .flat(flat));
        mMarkerCity.add(mConstanta);
        // Creates a draggable marker. Long press to drag.
        mBrasov = mMap.addMarker(new MarkerOptions()
                .position(BRASOV)
                .title("Brasov")
                .snippet("Population: 290,743")
                .draggable(true)
                .flat(flat));
        mMarkerCity.add(mBrasov);
        // Place four markers on top of each other with differing z-indexes.
        mTimis = mMap.addMarker(new MarkerOptions()
                .position(TIMISOARA)
                .title("Timisoara")
                .snippet("Population 332,983")
                .flat(flat));
        mMarkerCity.add(mTimis);
        mAlba = mMap.addMarker(new MarkerOptions()
                .position(ALBAIULIA)
                .title("Alba Iulia")
                .snippet("Population 63,536")
                .flat(flat));
        mMarkerCity.add(mAlba);

        mTgMures = mMap.addMarker(new MarkerOptions()
                .position(TARGUMURES)
                .title("Tg Mures")
                .snippet("Population 134,290")
                .flat(flat));
        mMarkerCity.add(mTgMures);
        // A few more markers for good measure.
        mOradea = mMap.addMarker(new MarkerOptions()
                .position(ORADEA)
                .title("Oradea")
                .snippet("Population: 222,736")
                .flat(flat));
        mMarkerCity.add(mOradea);
        mIasi = mMap.addMarker(new MarkerOptions()
                .position(IASI)
                .title("Iasi")
                .snippet("Population: 378,954")
                .flat(flat));
        mMarkerCity.add(mIasi);
        mSuceava = mMap.addMarker(new MarkerOptions()
                .position(SUCEAVA)
                .title("Suceava")
                .snippet("Population: 124,161")
                .flat(flat));
        mMarkerCity.add(mSuceava);
        mCluj = mMap.addMarker(new MarkerOptions()
                .position(CLUJ)
                .title("Cluj")
                .snippet("Population: 321,687")
                .flat(flat));
        mMarkerCity.add(mCluj);
    }

    /**
     * Demonstrates converting a {@link Drawable} to a {@link BitmapDescriptor},
     * for use as a marker icon.
     */
    private BitmapDescriptor vectorToBitmap(@DrawableRes int id, @ColorInt int color) {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), id, null);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * Called when the Clear button is clicked.
     */
    public void onClearMap(View view) {
        if (!checkReady()) {
            return;
        }
        mMap.clear();
    }

    /**
     * Called when the Reset button is clicked.
     */
    public void onResetMap(View view) {
        if (!checkReady()) {
            return;
        }
        // Clear the map because we don't want duplicates of the markers.
        mMap.clear();
        addMarkersToMap();
        groundOverlayRotated = mMap.addGroundOverlay(new GroundOverlayOptions()
                .image(images.get(1)).anchor((float) 0.7, (float) 0.3)
                .position(PARLAMENT, 750f, 420f)
                .bearing(7)
        );

        // Add a large overlay at Bucharest on top of the smaller overlay.
        groundOverlay = mMap.addGroundOverlay(new GroundOverlayOptions()
                .image(images.get(currentEntry)).anchor((float) 0.5, (float) 0.60)
                .position(BUCHAREST, 17500f, 16500f));
        transparencyBar.setOnSeekBarChangeListener(this);
    }

    /**
     * Called when the Reset button is clicked.
     */
    public void onToggleFlat(View view) {
        if (!checkReady()) {
            return;
        }
        boolean flat = mFlatBox.isChecked();
        for (Marker marker : mMarkerCity) {
            marker.setFlat(flat);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (!checkReady()) {
            return;
        }
        if (groundOverlay != null) {
            groundOverlay.setTransparency((float) progress / (float) TRANSPARENCY_MAX);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // Do nothing.
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // Do nothing.
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if (marker.equals(mOradea)) {
            // This causes the marker at Oradea to bounce into position when it is clicked.
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final long duration = 1500;

            final Interpolator interpolator = new BounceInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = Math.max(
                            1 - interpolator.getInterpolation((float) elapsed / duration), 0);
                    marker.setAnchor(0.5f, 1.0f + 2 * t);

                    if (t > 0.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    }
                }
            });
        } else if (marker.equals(mIasi)) {
            // This causes the marker at Iasi to change color and alpha.
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(mRandom.nextFloat() * 360));
            marker.setAlpha(mRandom.nextFloat());
        }

        // Markers have a z-index that is settable and gettable.
        float zIndex = marker.getZIndex() + 1.0f;
        marker.setZIndex(zIndex);
        Toast.makeText(this, marker.getTitle() + " z-index set to " + zIndex,
                Toast.LENGTH_SHORT).show();

        mLastSelectedMarker = marker;
        // We return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }

    //
    // Marker related listeners.
    //

    private City findCity(String nickname) {
        return DBDiscoverRomania.getsInstance(MarkerDemoActivity.this)
                .cityDAO()
                .getCity(nickname);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        final Activity mContext = MarkerDemoActivity.this;
        // Toast.makeText(this, "Click Info Window", Toast.LENGTH_SHORT).show();
        if (marker.equals(mBucharest)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Bucharest"));
            mContext.startActivity(intent);
        } else if (marker.equals(mIasi)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Iasi"));
            mContext.startActivity(intent);
        } else if (marker.equals(mConstanta)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Constanta"));
            mContext.startActivity(intent);
        } else if (marker.equals(mBrasov)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Brasov"));
            mContext.startActivity(intent);
        } else if (marker.equals(mOradea)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Oradea"));
            mContext.startActivity(intent);
        } else if (marker.equals(mTimis)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Timisoara"));
            mContext.startActivity(intent);
        } else if (marker.equals(mAlba)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Alba Iulia"));
            mContext.startActivity(intent);
        } else if (marker.equals(mTgMures)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Targu Mures"));
            mContext.startActivity(intent);
        } else if (marker.equals(mSuceava)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Suceava"));
            mContext.startActivity(intent);
        } else if (marker.equals(mCluj)) {
            Intent intent = FinalCityInfoActivity.getStartIntent(mContext, findCity("Cluj Napoca"));
            mContext.startActivity(intent);
        }
    }

    @Override
    public void onInfoWindowClose(Marker marker) {
        //Toast.makeText(this, "Close Info Window", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {
        //Toast.makeText(this, "Info Window long click", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        mTopText.setText("onMarkerDragStart");
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        mTopText.setText("onMarkerDragEnd");
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        mTopText.setText("onMarkerDrag.  Current Position: " + marker.getPosition());
    }

    /**
     * Toggles the visibility between 100% and 50% when a {@link GroundOverlay} is clicked.
     */
    @Override
    public void onGroundOverlayClick(GroundOverlay groundOverlay) {
        // Toggle transparency value between 0.0f and 0.5f. Initial default value is 0.0f.
        groundOverlayRotated.setTransparency(0.5f - groundOverlayRotated.getTransparency());
    }
/*
    public void switchImage(View view) {
        currentEntry = (currentEntry + 1) % images.size();
        groundOverlay.setImage(images.get(currentEntry));
    }*/

    /**
     * Demonstrates customizing the info window and/or its contents.
     */
    class CustomInfoWindowAdapter implements InfoWindowAdapter {

        // These are both viewgroups containing an ImageView with id "badge" and two TextViews with id
        // "title" and "snippet".
        private final View mWindow;

        private final View mContents;

        CustomInfoWindowAdapter() {
            mWindow = getLayoutInflater().inflate(R.layout.custom_info_window, null);
            mContents = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            if (mOptions.getCheckedRadioButtonId() != R.id.custom_info_window) {
                // This means that getInfoContents will be called.
                return null;
            }
            render(marker, mWindow);
            return mWindow;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (mOptions.getCheckedRadioButtonId() != R.id.custom_info_contents) {
                // This means that the default info contents will be used.
                return null;
            }
            render(marker, mContents);
            return mContents;
        }

        private void render(Marker marker, View view) {
            int badge;
            // Use the equals() method on a Marker to check for equals.  Do not use ==.
            if (marker.equals(mBucharest)) {
                badge = R.drawable.badge_buc;
            } else if (marker.equals(mIasi)) {
                badge = R.drawable.badge_is;
            } else if (marker.equals(mConstanta)) {
                badge = R.drawable.badge_ct;
            } else if (marker.equals(mBrasov)) {
                badge = R.drawable.badge_bv;
            } else if (marker.equals(mOradea)) {
                badge = R.drawable.badge_bh;
            } else if (marker.equals(mTimis)) {
                badge = R.drawable.badge_tm;
            } else if (marker.equals(mAlba)) {
                badge = R.drawable.badge_ab;
            } else if (marker.equals(mTgMures)) {
                badge = R.drawable.badge_tgm;
            } else if (marker.equals(mSuceava)) {
                badge = R.drawable.badge_sv;
            } else if (marker.equals(mCluj)) {
                badge = R.drawable.badge_cj;
            } else {
                // Passing 0 to setImageResource will clear the image view.
                badge = 0;
            }
            ((ImageView) view.findViewById(R.id.badge)).setImageResource(badge);

            String title = marker.getTitle();
            TextView titleUi = ((TextView) view.findViewById(R.id.title));
            if (title != null) {
                // Spannable string allows us to edit the formatting of the text.
                SpannableString titleText = new SpannableString(title);
                titleText.setSpan(new ForegroundColorSpan(Color.RED), 0, titleText.length(), 0);
                titleUi.setText(titleText);
            } else {
                titleUi.setText("");
            }

            String snippet = marker.getSnippet();
            TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
            if (snippet != null && snippet.length() > 12) {
                SpannableString snippetText = new SpannableString(snippet);
                snippetText.setSpan(new ForegroundColorSpan(Color.MAGENTA), 0, 10, 0);
                snippetText.setSpan(new ForegroundColorSpan(Color.BLUE), 12, snippet.length(), 0);
                snippetUi.setText(snippetText);
            } else {
                snippetUi.setText("");
            }
        }
    }

    /**
     * Toggles the clickability of the smaller, rotated overlay based on the state of the View that
     * triggered this call.
     * This callback is defined on the CheckBox in the layout for this Activity.
     *//*
    public void toggleClickability(View view) {
        if (groundOverlayRotated != null) {
            groundOverlayRotated.setClickable(((CheckBox) view).isChecked());
        }
    }*/

}