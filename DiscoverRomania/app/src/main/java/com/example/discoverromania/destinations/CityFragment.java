package com.example.discoverromania.destinations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.example.discoverromania.R;
import com.example.discoverromania.destinations.description.FinalCityInfoActivity;
import com.example.discoverromania.roompersistence.DBDiscoverRomania;
import com.example.discoverromania.roompersistence.DiscoverRomaniaViewModel;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import flipviewpage.utils.FlipSettings;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import objects.City;
import utils.DiscoverRomaniaSnackbars;

import static utils.Constants.DESCRIPTION_CITY;
import static utils.Constants.IMAGE_CITY;
import static utils.Constants.LATITUDE_CITY;
import static utils.Constants.LONGITUDE_CITY;
import static utils.Constants.NICKNAME_CITY;
import static utils.Constants.USER_TOKEN;

public class CityFragment extends Fragment implements DiscoverRomaniaSnackbars {

    private final int[] mColors = {R.color.sienna, R.color.saffron, R.color.green, R.color.pink,
            R.color.orange, R.color.blue, R.color.grey, R.color.yellow, R.color.purple, R.color.peach};
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.music_list)
    ListView lv;
    private MaterialSearchView mMaterialSearchView;
    private String mNameyet;
    private Activity mActivity;
    private Handler mHandler;
    private DiscoverRomaniaViewModel mViewModel;
    private String mToken;

    public CityFragment() {
    }

    public static CityFragment newInstance() {
        CityFragment fragment = new CityFragment();
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_citylist, container, false);

        ButterKnife.bind(this, view);

        // Hide keyboard
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(mActivity.getWindow().getDecorView().getWindowToken(), 0);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mToken = sharedPreferences.getString(USER_TOKEN, null);

        mHandler = new Handler(Looper.getMainLooper());

        mMaterialSearchView = view.findViewById(R.id.search_view);
        mMaterialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.v("QUERY ITEM : ", query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mNameyet = newText;
                if (!mNameyet.contains(" ")) { //&& mNameyet.length() % 2 == 0) {
                    cityAutoComplete();
                }
                return true;
            }
        });
        fetchCitiesList();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        mMaterialSearchView.setMenuItem(item);
    }

    private void cityAutoComplete() {
        if (mNameyet.trim().equals(""))
            return;

        final ArrayList<String> cityNames;
        cityNames = new ArrayList<>();

        List<City> filteredCities = DBDiscoverRomania.getsInstance(getContext())
                .cityDAO()
                .getSortedCitiesLike(mNameyet + "%");

        for (City city : filteredCities) {
            cityNames.add(city.getNickname());
        }

        ArrayAdapter<String> dataAdapter =
                new ArrayAdapter<>(
                        mActivity.getApplicationContext(), R.layout.spinner_layout, cityNames);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mMaterialSearchView.setAdapter(dataAdapter);
        mMaterialSearchView.setOnItemClickListener((arg0, arg1, arg2, arg3) -> {
            Intent intent = FinalCityInfoActivity.getStartIntent(mActivity, filteredCities.get(arg2));
            startActivity(intent);
        });
    }

    /**
     * Fetches the list of popular cities from server
     */

    private void fetchCitiesList() {
        FlipSettings settings = new FlipSettings.Builder().defaultPage().build();
        List<City> cities = new ArrayList<>(NICKNAME_CITY.size());
        for (int i = 0; i < NICKNAME_CITY.size(); i++) {
            City cityItem = new City(i + 1, NICKNAME_CITY.get(i), IMAGE_CITY.get(i), DESCRIPTION_CITY.get(i), LATITUDE_CITY.get(i), LONGITUDE_CITY.get(i));

            Completable.fromAction(() -> DBDiscoverRomania.getsInstance(getContext()).cityDAO().insertCities(cityItem))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
        animationView.setVisibility(View.GONE);
        mDisposable.add(
                DBDiscoverRomania.getsInstance(getContext())
                        .cityDAO()
                        .getSortedCities()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(citiesList -> lv.setAdapter(new CityAdapter(mActivity, citiesList, settings)))
        );
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.mActivity = (Activity) activity;
    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

}
