package com.example.discoverromania;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.room.Room;

import com.example.discoverromania.destinations.CityFragment;
import com.example.discoverromania.login.LoginActivity;
import com.example.discoverromania.roompersistence.DBDiscoverRomania;
import com.example.discoverromania.roompersistence.UserDAO;
import com.example.discoverromania.usertrips.MyTripsFragment;
import com.example.discoverromania.utilities.AboutUsFragment;
import com.example.discoverromania.utilities.UtilitiesFragment;
import com.facebook.login.LoginManager;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;

import discover.TravelFragment;
import utils.ImageBitmapString;

import static utils.Constants.USER_NAME;
import static utils.Constants.USER_TOKEN;

/**
 * Launcher Activity; Handles fragment changes;
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String travelShortcut = "com.example.discoverromania.TravelShortcut";
    private static final String myTripsShortcut = "com.example.discoverromania.MyTripsShortcut";
    private static final String utilitiesShortcut = "com.example.discoverromania.UtilitiesShortcut";
    UserDAO db;
    DBDiscoverRomania dataBase;
    private SharedPreferences mSharedPreferences;
    private int mPreviousMenuItemId;
    private String mToken;
    private String username;
    private DrawerLayout mDrawer;
    private Handler mHandler;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);
        username = mSharedPreferences.getString(USER_NAME, null);

        mPreviousMenuItemId = R.id.nav_city; // This is default item
        mHandler = new Handler(Looper.getMainLooper());

        dataBase = Room.databaseBuilder(this, DBDiscoverRomania.class, "discoverRomania.db").allowMainThreadQueries().build();

        db = dataBase.getUserDao();
        String imageURL = db.loadUserInfo(username).getImage();

        //Initially city fragment
        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = CityFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.inc, fragment).commit();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);

        // Get runtime permissions for Android M
        getRuntimePermissions();

        mDrawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                mDrawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        try {
            fillNavigationView(username, imageURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (getIntent() != null && getIntent().getAction() != null) {
            switch (getIntent().getAction()) {
                case travelShortcut:
                    fragment = TravelFragment.newInstance();
                    fragmentManager.beginTransaction().replace(R.id.inc, fragment).commit();
                    break;
                case myTripsShortcut:
                    fragment = MyTripsFragment.newInstance();
                    fragmentManager.beginTransaction().replace(R.id.inc, fragment).commit();
                    break;
                case utilitiesShortcut:
                    fragment = UtilitiesFragment.newInstance();
                    fragmentManager.beginTransaction().replace(R.id.inc, fragment).commit();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    // Change fragment on selecting naviagtion drawer item
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == mPreviousMenuItemId) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        int id = item.getItemId();
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (id) {
            case R.id.nav_travel:
                fragment = TravelFragment.newInstance();
                break;

            case R.id.nav_mytrips:
                fragment = MyTripsFragment.newInstance();
                break;

            case R.id.nav_city:
                fragment = CityFragment.newInstance();
                break;

            case R.id.nav_utility:
                fragment = UtilitiesFragment.newInstance();
                break;

            case R.id.nav_about_us:
                fragment = AboutUsFragment.newInstance();
                break;

            case R.id.nav_signout: {
                String email = mSharedPreferences.getString(USER_NAME, null);


                //set AlertDialog before signout
                ContextThemeWrapper crt = new ContextThemeWrapper(this, R.style.AlertDialog);
                AlertDialog.Builder builder = new AlertDialog.Builder(crt);
                builder.setMessage(R.string.signout_message)
                        .setPositiveButton(R.string.positive_button,
                                (dialog, which) -> {
                                    mSharedPreferences
                                            .edit()
                                            .putString(USER_TOKEN, null)
                                            .apply();
                                    LoginManager.getInstance().logOut();
                                    Intent i = LoginActivity.getStartIntent(MainActivity.this);
                                    startActivity(i);
                                    finish();
                                })
                        .setNegativeButton(android.R.string.cancel,
                                (dialog, which) -> {

                                });
                builder.create().show();
                break;
            }
            case R.id.nav_settings:
                fragment = SettingsFragment.newInstance();
                break;

        }

        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.inc, fragment).commit();
        }

        mDrawer.closeDrawer(GravityCompat.START);
        mPreviousMenuItemId = item.getItemId();
        return true;
    }

    private void getRuntimePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_CONTACTS,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.WAKE_LOCK,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.VIBRATE,
                }, 0);
            }
        }
    }

    private void fillNavigationView(String username, String imageURL) throws MalformedURLException {

        String loginType = db.loadUserInfo(username).getLoginType();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Get reference to the navigation view header and email textview
        View navigationHeader = navigationView.getHeaderView(0);
        TextView emailTextView = navigationHeader.findViewById(R.id.email);
        emailTextView.setText(username);

        ImageView imageView = navigationHeader.findViewById(R.id.image);
        if (loginType.equals("Facebook")) {
            URL imageFB = new URL("https://graph.facebook.com/" + imageURL + "/picture?type=large");
            Picasso.get().load(String.valueOf(imageFB)).into(imageView);
        } else {
            if (imageURL == null)
                Picasso.get().load(R.drawable.default_user_icon).into(imageView);
            else {
                Bitmap bitmap = ImageBitmapString.StringToBitMap(imageURL);
                imageView.setImageBitmap(bitmap);
            }
        }
        imageView.setVisibility(View.VISIBLE);
    }

    public void onClickProfile(View view) {
        Intent in = ProfileActivity.getStartIntent(MainActivity.this);
        startActivity(in);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataBase = Room.databaseBuilder(this, DBDiscoverRomania.class, "discoverRomania.db")
                .allowMainThreadQueries()
                .build();
        db = dataBase.getUserDao();
        String imageURL = db.loadUserInfo(username).getImage();
        try {
            fillNavigationView(mSharedPreferences.getString(USER_NAME, null),
                    imageURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        invalidateOptionsMenu();
    }

}
