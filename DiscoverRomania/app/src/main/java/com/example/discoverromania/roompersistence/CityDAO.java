package com.example.discoverromania.roompersistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import objects.City;

@Dao
public interface CityDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCities(City city);

    @Query("SELECT * FROM city ORDER BY id")
    Flowable<List<City>> getSortedCities();

    @Query("SELECT * FROM city WHERE LOWER(name) LIKE :nameOfCity")
    List<City> getSortedCitiesLike(String nameOfCity);

    @Query("SELECT latitude FROM city ORDER BY id")
    List<String> getCityLat();

    @Query("SELECT longitude FROM city ORDER BY id")
    List<String> getCityLong();

    @Query("SELECT * FROM city where name= :name")
    City getCity(String name);


    @Query("UPDATE city SET description = :des where name is :nameOfCity")
    void updateDescription(String nameOfCity, String des);

    @Query("DELETE FROM city where name is :nameOfCity")
    void deleteFromCity(String nameOfCity);

}
