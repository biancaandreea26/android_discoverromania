package com.example.discoverromania.roompersistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import objects.User;

@Dao
public interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

    @Query("SELECT * FROM users where username= :mail and password= :password")
    User getUser(String mail, String password);

    @Query("SELECT id, firstname,lastname,image,datejoined,status,loginType FROM users where username= :mail")
    User loadUserInfo(String mail);

    @Query("UPDATE users SET password = :pass where username is :uname")
    void updatePassword(String uname, String pass);

    @Query("DELETE FROM users where id is :uid")
    void deleteFromUsers(int uid);

    @Query("UPDATE users SET firstname= :firstName, lastname= :lastName where username is :uname")
    void updateName(String firstName, String lastName, String uname);

    @Query("UPDATE users SET status= :status where username is :uname")
    void updateStatus(String uname, String status);

    @Query("UPDATE users SET image = :img where username is :uname")
    void updateImage(String img, String uname);

    @Query("UPDATE users SET token = :token, loginType =:login where username is :uname")
    void updateTokenAndLoginType(String token, String login, String uname);


}
