package com.example.discoverromania;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.example.discoverromania.roompersistence.DBDiscoverRomania;
import com.example.discoverromania.roompersistence.UserDAO;
import com.example.discoverromania.utilities.FullScreenImage;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import utils.CircleImageView;
import utils.DiscoverRomaniaSnackbars;
import utils.ImageBitmapString;

import static utils.Constants.USER_NAME;
import static utils.Constants.USER_TOKEN;

public class ProfileActivity extends AppCompatActivity {

    //request code for picked image
    private static final int RESULT_PICK_IMAGE = 2;
    private static final int RESULT_TAKE_PICTURE = 1;
    @BindView(R.id.display_image)
    CircleImageView displayImage;
    @BindView(R.id.change_image)
    CircleImageView changeImage;
    @BindView(R.id.display_name)
    EditText displayName;
    @BindView(R.id.display_email)
    TextView emailId;
    @BindView(R.id.display_joining_date)
    TextView joiningDate;
    @BindView(R.id.display_status)
    EditText displayStatus;
    @BindView(R.id.ib_edit_display_name)
    ImageButton editDisplayName;
    @BindView(R.id.ib_edit_display_status)
    ImageButton editDisplayStatus;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.status_progress_bar)
    ProgressBar statusProgressBar;
    @BindView(R.id.name_progress_bar)
    ProgressBar nameProgressBar;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.status_character_count)
    TextView characterCount;
    private final TextWatcher mCountCharacters = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            characterCount.setText(String.valueOf(s.length()) + getString(R.string.status_character_limit));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    UserDAO db;
    private SharedPreferences mSharedPreferences;
    private boolean mFlagForDrawable = true;
    private String mUserStatus;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        animationView.setVisibility(View.GONE);
        DBDiscoverRomania dataBase = DBDiscoverRomania.getsInstance(ProfileActivity.this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String mToken = mSharedPreferences.getString(USER_TOKEN, null);
        String username = mSharedPreferences.getString(USER_NAME, null);

        try {
            getUserDetails(username);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        editDisplayName.setOnClickListener(v -> {
            if (mFlagForDrawable) {
                mFlagForDrawable = false;
                editDisplayName.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_black_24dp));
                displayName.setFocusableInTouchMode(true);
                displayName.setCursorVisible(true);
                displayName.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).showSoftInput(displayName, InputMethodManager.SHOW_IMPLICIT);
            } else {
                mFlagForDrawable = true;
                editDisplayName.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black_24dp));
                displayName.setFocusableInTouchMode(false);
                displayName.setCursorVisible(false);
                displayName.setVisibility(View.VISIBLE);
                setUserDetails(username);
                String fname = dataBase.getUserDao().loadUserInfo(username).getFirstName();
                String lname = dataBase.getUserDao().loadUserInfo(username).getLastName();
                String fullEditedName = fname + " " + lname;
                displayName.setText(fullEditedName);
                setTitle(fullEditedName);
                runOnUiThread(() -> {
                    statusProgressBar.setVisibility(View.GONE);
                    displayName.setVisibility(View.VISIBLE);
                });

                DiscoverRomaniaSnackbars.createSnackBar(findViewById(R.id.layout),
                        R.string.name_updated, Snackbar.LENGTH_SHORT).show();
            }
        });

        editDisplayStatus.setOnClickListener(v -> {
            if (mFlagForDrawable) {
                mFlagForDrawable = false;
                editDisplayStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_black_24dp));
                displayStatus.setFocusableInTouchMode(true);
                displayStatus.setCursorVisible(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).showSoftInput(displayStatus, InputMethodManager.SHOW_IMPLICIT);
                characterCount.setVisibility(View.VISIBLE);
            } else {
                mFlagForDrawable = true;
                editDisplayStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black_24dp));
                displayStatus.setFocusableInTouchMode(false);
                displayStatus.setCursorVisible(false);
                setUserStatus(username);
                characterCount.setVisibility(View.VISIBLE);
                String status = dataBase.getUserDao().loadUserInfo(username).getStatus();
                displayStatus.setText(status);
                displayStatus.setVisibility(View.VISIBLE);

                runOnUiThread(() -> {
                    statusProgressBar.setVisibility(View.GONE);
                    displayStatus.setVisibility(View.VISIBLE);
                });


            }
        });
        displayStatus.addTextChangedListener(mCountCharacters);
        changeImage.setOnClickListener(v -> {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            Intent accessCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.choose_an_option));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{accessCamera});
            startActivityForResult(chooserIntent, RESULT_TAKE_PICTURE);

        });

        //open profile image when clicked on it
        displayImage.setOnClickListener(v -> {
            String firstName = dataBase.getUserDao().loadUserInfo(username).getFirstName();
            String lastName = dataBase.getUserDao().loadUserInfo(username).getLastName();
            String fullName = firstName + " " + lastName;
            Intent fullScreenIntent = FullScreenImage.getStartIntent(ProfileActivity.this, fullName);
            fullScreenIntent.putExtra("USERNAME", username);
            startActivity(fullScreenIntent);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        DBDiscoverRomania dataBase = DBDiscoverRomania.getsInstance(ProfileActivity.this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = mSharedPreferences.getString(USER_NAME, null);

        if (data == null)
            return;

        //After user has picked the image
        System.out.println("request code   " + requestCode);
        System.out.println("result code   " + resultCode);
        if (requestCode == RESULT_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            //startCropIntent(selectedImage);
            CropImage.activity(selectedImage).start(this);
        }
        //After user has cropped the image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri croppedImage = result.getUri();
                Picasso.get().load(croppedImage).into(displayImage);
                InputStream is = null;
                try {
                    is = getContentResolver().openInputStream(croppedImage);
                    Bitmap bitmap = BitmapFactory.decodeStream(is);
                    String imageSource = ImageBitmapString.BitMapToString(bitmap);
                    dataBase.getUserDao().updateImage(imageSource, username);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                DiscoverRomaniaSnackbars.createSnackBar(findViewById(R.id.layout), R.string.profile_picture_updated,
                        Snackbar.LENGTH_SHORT).show();

            }
        }

    }

    private void getUserDetails(String email) throws MalformedURLException {
        DBDiscoverRomania dataBase = DBDiscoverRomania.getsInstance(ProfileActivity.this);
        String firstName = dataBase.getUserDao().loadUserInfo(email).getFirstName();
        String lastName = dataBase.getUserDao().loadUserInfo(email).getLastName();
        String imageUrl = dataBase.getUserDao().loadUserInfo(email).getImage();
        String dateJoined = dataBase.getUserDao().loadUserInfo(email).getDateJoined();
        String status = dataBase.getUserDao().loadUserInfo(email).getStatus();
        String loginType = dataBase.getUserDao().loadUserInfo(email).getLoginType();
        String fullName = firstName + " " + lastName;
        fillProfileInfo(fullName, email, imageUrl, dateJoined, status, loginType);
    }

    private void fillProfileInfo(String fullName, String email, String imageURL,
                                 String dateJoined, String status, String loginType) throws MalformedURLException {

        displayImage.setVisibility(View.VISIBLE);
        displayName.setText(fullName);
        emailId.setText(email);
        joiningDate.setText(String.format(getString(R.string.text_joining_date), dateJoined));
        if (loginType.equals("Facebook")) {
            URL imageFB = new URL("https://graph.facebook.com/" + imageURL + "/picture?type=large");
            Picasso.get().load(String.valueOf(imageFB)).into(displayImage);
        } else {
            if (imageURL == null)
                Picasso.get().load(R.drawable.default_user_icon).into(displayImage);
            else {
                Bitmap bitmap = ImageBitmapString.StringToBitMap(imageURL);
                displayImage.setImageBitmap(bitmap);
            }
        }

        setTitle(fullName);
        if (status == null)
            status = getString(R.string.default_status);
        displayStatus.setText(status);
    }

    private void setUserDetails(String email) {

        DBDiscoverRomania dataBase = DBDiscoverRomania.getsInstance(ProfileActivity.this);

        runOnUiThread(() -> {
            displayName.setVisibility(View.VISIBLE);
            nameProgressBar.setVisibility(View.GONE);
        });

        String fullName = String.valueOf(displayName.getText());
        String firstName = fullName.substring(0, fullName.indexOf(' '));
        String lastName = fullName.substring(fullName.indexOf(' ') + 1);

        dataBase.getUserDao().updateName(firstName, lastName, email);
    }

    private void setUserStatus(String email) {

        DBDiscoverRomania dataBase = DBDiscoverRomania.getsInstance(ProfileActivity.this);
        runOnUiThread(() -> {
            displayStatus.setVisibility(View.INVISIBLE);
            statusProgressBar.setVisibility(View.INVISIBLE);
        });

        mUserStatus = String.valueOf(displayStatus.getText());

        if (mUserStatus.equals("")) {

            mUserStatus = getString(R.string.default_status);

        } else {
            dataBase.getUserDao().updateStatus(email, mUserStatus);


        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View view = getCurrentFocus();
            if (view instanceof EditText) {
                Rect outRect = new Rect();
                view.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    view.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

}