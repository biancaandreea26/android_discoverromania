package com.example.discoverromania.roompersistence;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import objects.ChecklistItem;

/**
 * Contains queries on database
 */
@Dao
public interface ChecklistItemDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertItems(ChecklistItem items);

    @Query("SELECT * FROM events_new ORDER BY isDone")
    Flowable<List<ChecklistItem>> getSortedItems();

    @Query("UPDATE events_new SET isDone = 1 WHERE id IS :id")
    void updateIsDone(int id);

    @Query("UPDATE events_new SET isDone = 0 WHERE id IS :id")
    void updateUndone(int id);

    @Query("DELETE FROM events_new WHERE isDone = 1")
    void deleteCompletedTasks();

    @Query("SELECT * FROM events_new WHERE isDone = 1")
    Single<List<ChecklistItem>> getCompletedItems();

}
