package com.example.discoverromania.roompersistence;


import androidx.lifecycle.ViewModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import objects.ChecklistItem;
import objects.City;
import objects.User;


public class DiscoverRomaniaViewModel extends ViewModel {


    private final DiscoverRomaniaDataSource mDataSource;

    public DiscoverRomaniaViewModel(DiscoverRomaniaDataSource dataSource) {
        mDataSource = dataSource;
    }

    public Flowable<List<ChecklistItem>> getSortedItems() {
        return mDataSource.getSortedItems();
    }

    public Completable updateIsDone(final int id) {
        return Completable.fromAction(() -> mDataSource.updateIsDone(id));
    }

    public Completable updateUndone(final int id) {
        return Completable.fromAction(() -> mDataSource.updateUndone(id));
    }

    public Completable insertItem(ChecklistItem item) {
        return Completable.fromAction(() -> mDataSource.insertItem(item));
    }

    public Completable deleteCompletedTasks() {
        return Completable.fromAction(mDataSource::deleteCompletedTasks);
    }

    public Single<List<ChecklistItem>> getCompletedItems() {
        return mDataSource.getCompletedItems();
    }

    public Flowable<List<City>> getSortedCities() {
        return mDataSource.getSortedCities();
    }

    public Completable insertCities(City city) {
        return Completable.fromAction(() -> mDataSource.insertCities(city));
    }

    public Completable updateDescription(String cityName, String description) {
        return Completable.fromAction(() -> mDataSource.updateDescription(cityName, description));
    }

    public Completable deleteFromCity(String cityName) {
        return Completable.fromAction(() -> mDataSource.deleteFromCity(cityName));
    }

    public Completable loadUser(String username, String password) {
        return Completable.fromAction(() -> mDataSource.getUser(username, password));
    }

    public Completable updatePassword(String username, String password) {
        return Completable.fromAction(() -> mDataSource.updatePassword(username, password));
    }

    public Completable insertUser(User user) {
        return Completable.fromAction(() -> mDataSource.insertUser(user));
    }

    public Completable deleteFromUser(int id) {
        return Completable.fromAction(() -> mDataSource.deleteFromUsers(id));
    }

}
