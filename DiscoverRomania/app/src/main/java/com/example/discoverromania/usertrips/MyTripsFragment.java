package com.example.discoverromania.usertrips;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.example.discoverromania.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import objects.Trip;
import utils.DiscoverRomaniaSnackbars;

import static android.app.Activity.RESULT_OK;
import static utils.Constants.USER_TOKEN;

//import static utils.Constants.API_LINK_V2;

public class MyTripsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, DiscoverRomaniaSnackbars {

    static int ADDNEWTRIP_ACTIVITY = 203;
    private final List<Trip> mTrips = new ArrayList<>();
    @BindView(R.id.gv)
    GridView gridView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private String mToken;
    private Handler mHandler;
    private Activity mActivity;
    private TripsListAdapter mMyTripsAdapter;
    private View mTripsView;

    public MyTripsFragment() {
        // Required empty public constructor
    }

    public static MyTripsFragment newInstance() {
        return new MyTripsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mTripsView = inflater.inflate(R.layout.fragment_my_trips, container, false);
        ButterKnife.bind(this, mTripsView);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mToken = sharedPreferences.getString(USER_TOKEN, null);
        mHandler = new Handler(Looper.getMainLooper());
        swipeRefreshLayout.setOnRefreshListener(this);
        //mytrip();
        return mTripsView;

    }

    @OnClick(R.id.add_trip)
    void addTrip() {
        Intent intent = new Intent(getContext(), AddNewTripActivity.class);
        startActivityForResult(intent, ADDNEWTRIP_ACTIVITY);

    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    /**
     * Plays the no results animation in the view
     */
    private void noResults() {
        DiscoverRomaniaSnackbars.createSnackBar(mTripsView.findViewById(R.id.my_trips_frag), R.string.no_trips,
                Snackbar.LENGTH_LONG).show();
        animationView.setAnimation(R.raw.empty_list);
        animationView.playAnimation();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.mActivity = (Activity) activity;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADDNEWTRIP_ACTIVITY && resultCode == RESULT_OK) {
            mTrips.clear();
            mMyTripsAdapter.notifyDataSetChanged();
            networkError();

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTrips.clear();
    }

    @Override
    public void onRefresh() {
        mTrips.clear();
        mMyTripsAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }
}
