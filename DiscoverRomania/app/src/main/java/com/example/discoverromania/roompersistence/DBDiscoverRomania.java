package com.example.discoverromania.roompersistence;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {objects.City.class, objects.User.class, objects.ChecklistItem.class}, version = 4, exportSchema = false)
public abstract class DBDiscoverRomania extends RoomDatabase {

    //migration from database version 3 to 4
    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Create the new table
            database.execSQL(
                    "CREATE TABLE checklist_items (id INTEGER PRIMARY KEY NOT NULL, name TEXT," +
                            " isDone TEXT)");
            // Copy the data
            database.execSQL(
                    "INSERT INTO checklist_items (id, name, isDone) " +
                            "SELECT id, name, isDone FROM events_new");
            // Remove the old table
            database.execSQL("DROP TABLE events_new");
            // Change the table name to the correct one
            database.execSQL("ALTER TABLE checklist_items RENAME TO events_new");
        }
    };
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "discoverRomania.db";
    private static DBDiscoverRomania sInstance;

    public static DBDiscoverRomania getsInstance(Context context) {
        //to make sure that Singleton Pattern is followed
        //i.e. only one object of the class is created.
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        DBDiscoverRomania.class, DBDiscoverRomania.DATABASE_NAME)
                        .addMigrations(MIGRATION_3_4)
                        .allowMainThreadQueries()
                        .build();
            }
        }
        return sInstance;
    }

    public abstract ChecklistItemDAO checklistItemDAO();

    public abstract CityDAO cityDAO();

    public abstract UserDAO getUserDao();

}
