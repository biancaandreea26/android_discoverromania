package com.example.discoverromania.usertrips;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.discoverromania.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import objects.Trip;

class TripsListAdapter extends ArrayAdapter<Trip> {
    private final Context mContext;
    private final List<Trip> mTrips;
    private LayoutInflater mInflater;

    TripsListAdapter(Context context,
                     List<Trip> trips) {
        super(context, R.layout.trip_listitem, trips);
        this.mContext = context;
        this.mTrips = trips;
        mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }


    class ViewHolder {

        @BindView(R.id.profile_image)
        ImageView city;
        @BindView(R.id.tv)
        TextView cityname;
        @BindView(R.id.date)
        TextView date;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}