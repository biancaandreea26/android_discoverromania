package com.example.discoverromania.destinations.description;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.example.discoverromania.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import objects.City;

import static utils.Constants.EXTRA_MESSAGE_CITY_OBJECT;
import static utils.Constants.USER_TOKEN;


/**
 * Fetch city information for given city mId
 */
public class FinalCityInfoActivity extends AppCompatActivity {

    @BindView(R.id.layout_content)
    LinearLayout content;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.head)
    TextView title;
    @BindView(R.id.image_slider)
    ImageView imagesSliderView;
    @BindView(R.id.SliderDots)
    LinearLayout sliderDotsPanel;
    @BindView(R.id.description)
    TextView description;
    int currentPage = 0;
    Timer timer;
    private int mDotsCount;
    private ImageView[] mDots;
    private Handler mHandler;
    private City mCity;
    private String mToken;
    private String mCurrentTemp;
    private boolean mIsExpandClicked = false;

    public static Intent getStartIntent(Context context, City city) {
        Intent intent = new Intent(context, FinalCityInfoActivity.class);
        intent.putExtra(EXTRA_MESSAGE_CITY_OBJECT, city);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_city_info);
        ButterKnife.bind(this);


        mHandler = new Handler(Looper.getMainLooper());

        Intent intent = getIntent();
        mCity = (City) intent.getSerializableExtra(EXTRA_MESSAGE_CITY_OBJECT);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = sharedPreferences.getString(USER_TOKEN, null);

        initUi();
    }

    /**
     * Initialize view items with information
     * received from previous intent
     */
    private void initUi() {

        setTitle(mCity.getNickname());
        title.setText(mCity.getNickname());
        animationView.setVisibility(View.GONE);
        Picasso.get().
                load(mCity.getAvatar()).
                placeholder(R.drawable.placeholder_image).
                into(imagesSliderView);
        description.setText(mCity.getDescription());
        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void parseInfoResult(final String description,
                                final String latitude,
                                final String longitude,
                                ArrayList<String> imagesArray) {
        mHandler.post(() -> {
            Log.e("description", description + " ");
            animationView.setVisibility(View.GONE);
            content.setVisibility(View.VISIBLE);
            //if (description != null && !description.equals("null"))
            //cityDescription.setText(description);
            mCity.setDescription(description);
            mCity.setLatitude(latitude);
            mCity.setLongitude(longitude);
            //if (imagesArray.size() > 0)
            // slideImages(imagesArray);
        });
    }

    /**
     * Plays the network lost animation in the view
     */
    public void networkError() {
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }
}