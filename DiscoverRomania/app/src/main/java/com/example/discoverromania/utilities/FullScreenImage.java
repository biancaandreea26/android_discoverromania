package com.example.discoverromania.utilities;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.discoverromania.R;
import com.example.discoverromania.roompersistence.DBDiscoverRomania;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;
import utils.ImageBitmapString;
import utils.TouchImageView;

import static utils.Constants.EXTRA_MESSAGE_IMAGE_URI;
import static utils.Constants.EXTRA_MESSAGE_USER_FULLNAME;

public class FullScreenImage extends AppCompatActivity {

    @BindView(R.id.full_profile_image)
    TouchImageView fullProfileImage;

    public static Intent getStartIntent(Context context, String title) {
        Intent intent = new Intent(context, FullScreenImage.class);
        //      intent.putExtra(EXTRA_MESSAGE_IMAGE_URI, uri);
        intent.putExtra(EXTRA_MESSAGE_USER_FULLNAME, title);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_profile_image);
        ButterKnife.bind(this);

        DBDiscoverRomania dataBase = DBDiscoverRomania.getsInstance(FullScreenImage.this);
        String username = getIntent().getStringExtra("USERNAME");

        String imageUrl = dataBase.getUserDao().loadUserInfo(username).getImage();

        Intent intent = getIntent();
        String imageUri = (String) intent.getSerializableExtra(EXTRA_MESSAGE_IMAGE_URI);
        String title = (String) intent.getSerializableExtra(EXTRA_MESSAGE_USER_FULLNAME);

        if (imageUri == null)
            Picasso.get().load(R.drawable.default_user_icon);
        else {
            Bitmap bitmap = ImageBitmapString.StringToBitMap(imageUrl);

            File file = createCustomFile(username);
            try {
                saveReceivedImage(file, bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            fullProfileImage.setVisibility(View.VISIBLE);
            Picasso.get().load(username + "jpeg").into(fullProfileImage);


        }


        setTitle(title);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //menu item selected
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private File createCustomFile(String fileName) {

        ContextWrapper context = new ContextWrapper(getApplicationContext());
        File path = new File(context.getFilesDir(), "DiscoverRomania" + File.separator + "Images");
        if (!path.exists()) {
            path.mkdirs();
        }
        File outFile = new File(path, fileName + ".jpeg");

        return outFile;

    }

    private void saveReceivedImage(File file, Bitmap bitmap) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        outputStream.close();

    }
}
