package com.example.discoverromania.roompersistence;

import android.content.Context;

/**
 * Enables injection of data sources.
 */
public class Injection {

    public static DiscoverRomaniaDataSource provideUserDataSource(Context context) {
        DBDiscoverRomania database = DBDiscoverRomania.getsInstance(context);
        return new DiscoverRomaniaDataSource(database.checklistItemDAO(), database.cityDAO(), database.getUserDao());
    }

    public static ViewModelFactory provideViewModelFactory(Context context) {
        DiscoverRomaniaDataSource dataSource = provideUserDataSource(context);
        return new ViewModelFactory(dataSource);
    }
}
