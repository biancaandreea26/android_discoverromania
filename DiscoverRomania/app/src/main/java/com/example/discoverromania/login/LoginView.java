package com.example.discoverromania.login;


interface LoginView {

    void rememberUserInfo(String token, String email);

    void startMainActivity();

    void showError();

    void showLoadingDialog();

    void dismissLoadingDialog();

    void getRunTimePermissions();

    void checkUserSession();


}
