package com.example.discoverromania.roompersistence;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import objects.ChecklistItem;
import objects.City;
import objects.User;


public class DiscoverRomaniaDataSource {

    private final ChecklistItemDAO mDao;
    private CityDAO mCityDao;
    private UserDAO mUserDao;

    public DiscoverRomaniaDataSource(ChecklistItemDAO checklistItemDAO, CityDAO cityDao, UserDAO userDAO) {
        mDao = checklistItemDAO;
        mCityDao = cityDao;
        mUserDao = userDAO;
    }

    Flowable<List<ChecklistItem>> getSortedItems() {
        return mDao.getSortedItems();
    }

    void updateIsDone(int id) {
        mDao.updateIsDone(id);
    }

    void updateUndone(int id) {
        mDao.updateUndone(id);
    }

    void insertItem(ChecklistItem item) {
        mDao.insertItems(item);
    }

    void deleteCompletedTasks() {
        mDao.deleteCompletedTasks();
    }

    Single<List<ChecklistItem>> getCompletedItems() {
        return mDao.getCompletedItems();
    }

    /**
     * User
     */
    void getUser(String username, String password) {
        mUserDao.getUser(username, password);
    }

    void updatePassword(String uname, String pass) {
        mUserDao.updatePassword(uname, pass);
    }

    void insertUser(User user) {
        mUserDao.insertUser(user);
    }

    void deleteFromUsers(int id) {
        mUserDao.deleteFromUsers(id);
    }

    /**
     * City
     */

    void insertCities(City city) {
        mCityDao.insertCities(city);
    }

    void updateDescription(String nameOfCity, String des) {
        mCityDao.updateDescription(nameOfCity, des);
    }

    void deleteFromCity(String nameOfCity) {
        mCityDao.deleteFromCity(nameOfCity);
    }

    Flowable<List<City>> getSortedCities() {
        return mCityDao.getSortedCities();
    }


}
