package com.example.discoverromania.usertrips;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.dd.processbutton.FlatButton;
import com.example.discoverromania.R;
import com.example.discoverromania.searchcitydialog.CitySearchDialogCompat;
import com.example.discoverromania.searchcitydialog.CitySearchModel;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import utils.DiscoverRomaniaSnackbars;
import utils.Utils;

import static utils.Constants.USER_TOKEN;

//import static utils.Constants.API_LINK_V2;

/**
 * Activity to add new trip
 */
public class AddNewTripActivity extends AppCompatActivity implements DiscoverRomaniaSnackbars, DatePickerDialog.OnDateSetListener, View.OnClickListener {
    // View.OnClickListener, DiscoverRomaniaSnackbars {

    private static final String DATEPICKER_TAG1 = "datepicker1";
    @BindView(R.id.select_city_name)
    TextView cityName;
    @BindView(R.id.sdate)
    TextView tripStartDate;
    @BindView(R.id.ok)
    FlatButton ok;
    @BindView(R.id.tname)
    EditText tripName;
    @BindView(R.id.linear_layout)
    LinearLayout mLinearLayout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    private String mCityid;
    private String mStartdate;
    private String mTripname;
    private String mToken;
    private MaterialDialog mDialog;
    private Handler mHandler;
    private DatePickerDialog mDatePickerDialog;
    private ArrayList<CitySearchModel> mSearchCities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_trip);

        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mHandler = new Handler(Looper.getMainLooper());
        mToken = sharedPreferences.getString(USER_TOKEN, null);

        final Calendar calendar = Calendar.getInstance();

        mDatePickerDialog = new DatePickerDialog(
                AddNewTripActivity.this,
                AddNewTripActivity.this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        tripStartDate.setOnClickListener(this);
        ok.setOnClickListener(this);
        cityName.setOnClickListener(this);

        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            // Set Start date
            case R.id.sdate:
                mDatePickerDialog.show();
                break;
            // Add a new trip
            case R.id.ok:
                Utils.hideKeyboard(this);
                mTripname = tripName.getText().toString();

                if (mTripname.trim().equals("")) {
                    DiscoverRomaniaSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                            R.string.trip_name_blank, Snackbar.LENGTH_LONG).show();
                } else if (tripStartDate == null || tripStartDate.getText().toString().equals("")) {
                    DiscoverRomaniaSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                            R.string.trip_date_blank, Snackbar.LENGTH_LONG).show();
                } else if (mCityid == null) {
                    DiscoverRomaniaSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                            R.string.trip_city_blank, Snackbar.LENGTH_LONG).show();
                } else
                    //addTrip();
                    networkError();

                break;
            case R.id.select_city_name:
                new CitySearchDialogCompat(AddNewTripActivity.this, getString(R.string.search_title),
                        getString(R.string.search_hint), null, mSearchCities,
                        (SearchResultListener<CitySearchModel>) (dialog, item, position) -> {
                            mCityid = item.getId();
                            cityName.setText(item.getTitle());
                            dialog.dismiss();
                        }).show();
                networkError();
                break;

        }
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, AddNewTripActivity.class);
        return intent;
    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        mLinearLayout.setVisibility(View.INVISIBLE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, month, dayOfMonth);
        Log.d("Month", String.valueOf(month));
        mStartdate = Long.toString(calendar.getTimeInMillis() / 1000);
        tripStartDate.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
    }
}