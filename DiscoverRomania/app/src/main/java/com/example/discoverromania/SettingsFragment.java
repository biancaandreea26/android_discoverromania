package com.example.discoverromania;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.airbnb.lottie.LottieAnimationView;
import com.dd.processbutton.iml.ActionProcessButton;
import com.example.discoverromania.login.LoginActivity;
import com.example.discoverromania.roompersistence.DBDiscoverRomania;
import com.example.discoverromania.roompersistence.UserDAO;
import com.google.android.material.snackbar.Snackbar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;

import static com.example.discoverromania.login.RegisterActivity.md5;
import static utils.Constants.READ_NOTIF_STATUS;
import static utils.Constants.USER_NAME;
import static utils.Constants.USER_TOKEN;

public class SettingsFragment extends Fragment {


    @BindView(R.id.old_password)
    EditText oldPasswordText;
    @BindView(R.id.new_password)
    EditText newPasswordText;
    @BindView(R.id.connfirm_password)
    EditText confirmPasswordText;
    @BindView(R.id.done_button)
    ActionProcessButton doneButton;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    UserDAO db;
    DBDiscoverRomania dataBase;
    private String mToken;
    private Handler mHandler;
    private Activity mActivity;
    private SharedPreferences mSharedPreferences;
    private View mView;
    private String username;


    public SettingsFragment() {
        //required public constructor
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, mView);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        boolean readNotifStatus = mSharedPreferences.getBoolean(READ_NOTIF_STATUS, true);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);
        username = mSharedPreferences.getString(USER_NAME, null);
        mHandler = new Handler(Looper.getMainLooper());

        doneButton.setMode(ActionProcessButton.Mode.ENDLESS);
        doneButton.setOnClickListener(v -> {
            hideKeyboard();
            if (checkEmptyText())
                checkPasswordMatch();
        });
        return mView;
    }

    /**
     * Checks if any editText is left empty by user
     *
     * @return true if all fields are filled
     */
    private boolean checkEmptyText() {
        if (oldPasswordText.getText().toString().equals("")) {
            oldPasswordText.setError(getString(R.string.required_error));
            oldPasswordText.requestFocus();
            return false;
        } else if (newPasswordText.getText().toString().equals("")) {
            newPasswordText.setError(getString(R.string.required_error));
            newPasswordText.requestFocus();
            return false;
        } else if (confirmPasswordText.getText().toString().equals("")) {
            confirmPasswordText.setError(getString(R.string.required_error));
            confirmPasswordText.requestFocus();
            return false;
        } else {
            return true;
        }

    }

    /**
     * Checks if newPassword and confirmPassword match
     */
    private void checkPasswordMatch() {

        String newPassword = newPasswordText.getText().toString();
        String confirmPassword = confirmPasswordText.getText().toString();
        if (validatePassword(newPassword)) {
            if (newPassword.equals(confirmPassword)) {
                changePassword(newPassword, username);
            } else {
                Snackbar snackbar = Snackbar
                        .make(mActivity.findViewById(android.R.id.content),
                                R.string.passwords_check, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    /**
     * Validates the given password, checks if given password proper format as standard password string
     *
     * @param passString password string to be validate
     * @return Boolean returns true if email format is correct, otherwise false
     */
    public boolean validatePassword(String passString) {
        if (passString.length() >= 8) {
            Pattern pattern;
            Matcher matcher;
            final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#:$%^&+=!])(?=\\S+$).{4,}$";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(passString);

            if (matcher.matches()) {
                return true;
            } else {
                Snackbar snackbar = Snackbar
                        .make(mActivity.findViewById(android.R.id.content),
                                R.string.invalid_password, Snackbar.LENGTH_LONG);
                snackbar.show();
                return false;
            }
        } else {
            Snackbar snackbar = Snackbar
                    .make(mActivity.findViewById(android.R.id.content),
                            R.string.password_length, Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }
    }


    public void changePassword(String newPassword, String username) {
        //  networkError();
        dataBase = Room.databaseBuilder(getContext(), DBDiscoverRomania.class, "discoverRomania.db").allowMainThreadQueries().build();
        db = dataBase.getUserDao();
        doneButton.setOnClickListener(v -> {
            db.updatePassword(username, md5(newPassword));
            Snackbar snackbar = Snackbar
                    .make(mActivity.findViewById(android.R.id.content),
                            "Password updated", Snackbar.LENGTH_LONG);
            snackbar.show();
            mSharedPreferences
                    .edit()
                    .putString(USER_TOKEN, null)
                    .apply();
            Intent loginActivity = LoginActivity.getStartIntent(getContext());
            startActivity(loginActivity);

        });
    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        layout.setVisibility(View.INVISIBLE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
    }

    private void clearText() {
        oldPasswordText.setText("");
        newPasswordText.setText("");
        confirmPasswordText.setText("");
    }

}
