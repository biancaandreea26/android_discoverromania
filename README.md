Numele aplicației: Discover Romania
Membrii echipei: Brănoiu Bianca-Irina și Barcan Andreea-Bianca
Specializarea BDTS, grupa 405

Cerințele implementate in proiect:

Android cerinte nota 5:
Implementat o operatie cu camera (facut poze) 
Implementat un Recycler View cu functie de cautare 
Utilizat o metoda de navigatie: Navigation Drawer sau Bottom Navigation 
Implementat o metoda de Share (Android Sharesheet) 

Optionale:
UI adaptat pentru landscape mode
Persistenta datelor folosind baze de date (Realm || Room) Room 
Maps: permisiune de la utilizator + overlays and markers + detail activity 
Social Login (Facebook)
Implementat o animatie folosind ObjectAnimator si una folosind MotionLayout

